It sounds like you're trying to describe a specific tune or melody. The "durrr-duh-duh-duh-duh-durrrrr-durrrr" and "laaa la-lal-lu-laaaaaar" are both onomatopoeic representations of music, but without more context, it's hard to pinpoint exactly what song or piece you're referring to.

If you can provide more details about the genre, artist, or any lyrics associated with the tune, it might help narrow it down. Alternatively, if there's a particular scene from a movie or TV show where this music is featured, that could also be useful information!

# Sat  6 Jul 19:15:13 CEST 2024 - That would really be more of a 'durrr-duh-duh-duh-duh-durrrrr-durrrr'. We're looking for 'laaa la-lal-lu-laaaaaar'